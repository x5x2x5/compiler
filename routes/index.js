const express = require('express');
const router = express.Router();
const { exec } = require('child_process');
const fs = require('fs');

router.get('/', function(req, res, next) {
	return res.render('index');
});

router.post('/result', function(req, res, next) {
	const input = req.body.input

	fs.writeFile("temp/demo.cpp", input, function(err) {
		
		if(err) {
			return res.render('index', { err,input });
		}

		exec('g++ -S temp/demo.cpp -o temp/demo.o', ( err, stdout, stderr ) => {
			if (err || stderr) {
				return res.render('index', { err, input, stderr });
			}

			fs.readFile('temp/demo.o', 'utf-8', function(err, data) {
				return res.render('index', {err, data, input });
			});
		});
	})
});

module.exports = router;